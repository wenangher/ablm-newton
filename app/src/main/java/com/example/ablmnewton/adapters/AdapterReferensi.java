package com.example.ablmnewton.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;

import java.util.List;

public class AdapterReferensi extends RecyclerView.Adapter<AdapterReferensi.ViewHolder> {

    private String [] arrReferensi;

    public AdapterReferensi(String[] arrReferensi)
    {
        this.arrReferensi = arrReferensi;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_referensi, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvReferensi.setText(arrReferensi[position]);
    }

    @Override
    public int getItemCount() {
        return arrReferensi.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvReferensi;

        public ViewHolder(View itemView) {
            super(itemView);

            tvReferensi = (TextView) itemView.findViewById(R.id.tv_referensi);
        }
    }
}
