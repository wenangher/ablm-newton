package com.example.ablmnewton.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.ActivityVideoRealPBL;
import com.example.ablmnewton.data.DataRealPBL;

import java.util.List;

public class AdapterRealPBL extends RecyclerView.Adapter<AdapterRealPBL.ViewHolder> {

    private List<DataRealPBL> data;
    private Activity activity;

    public AdapterRealPBL(Activity activity, List<DataRealPBL> data)
    {
        this.activity = activity;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_realpbl, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tvJudul.setText(data.get(position).getJudul());
        holder.imgRealPBL.setImageResource(data.get(position).getGambarPBL());
        holder.layRealPBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity.getApplicationContext(), ActivityVideoRealPBL.class);
                intent.putExtra("videopbl", position);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvJudul;
        ImageView imgRealPBL;
        LinearLayout layRealPBL;

        public ViewHolder(View itemView) {
            super(itemView);

            tvJudul = (TextView) itemView.findViewById(R.id.tv_realpbl);
            imgRealPBL = (ImageView) itemView.findViewById(R.id.img_realpbl);
            layRealPBL = (LinearLayout) itemView.findViewById(R.id.lay_realpbl);
        }
    }
}
