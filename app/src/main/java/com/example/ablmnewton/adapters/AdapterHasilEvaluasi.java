package com.example.ablmnewton.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.data.DataHasilEvaluasi;

import java.util.List;

public class AdapterHasilEvaluasi extends RecyclerView.Adapter<AdapterHasilEvaluasi.ViewHolder> {

    private List<DataHasilEvaluasi> data;
    private Activity activity;

    public AdapterHasilEvaluasi(Activity activity, List<DataHasilEvaluasi> data)
    {
        this.activity = activity;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_evaluasi, parent, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tvNama.setText(data.get(position).getNama());
        holder.tvNIS.setText(data.get(position).getNis());
        holder.tvNilai1.setText("" + data.get(position).getNilai1());
        holder.tvNilai2.setText("" + data.get(position).getNilai2());
        holder.tvNilai3.setText("" + data.get(position).getNilai3());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvNama, tvNIS,
                 tvNilai1, tvNilai2, tvNilai3;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNama      = (TextView) itemView.findViewById(R.id.tv_nama);
            tvNIS       = (TextView) itemView.findViewById(R.id.tv_nis);
            tvNilai1    = (TextView) itemView.findViewById(R.id.tv_nilai_eva1);
            tvNilai2    = (TextView) itemView.findViewById(R.id.tv_nilai_eva2);
            tvNilai3    = (TextView) itemView.findViewById(R.id.tv_nilai_eva3);
        }
    }
}
