package com.example.ablmnewton.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.materi.ActivitySoal3;

import java.util.List;

public class AdapterSoal extends RecyclerView.Adapter<AdapterSoal.ViewHolder> {

    private Activity activity;
    private List<String> listSoal;
    private String jenis;

    public AdapterSoal(Activity activity, List<String> listSoal, String jenis)
    {
        this.activity = activity;
        this.listSoal = listSoal;
        this.jenis = jenis;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_contoh_soal, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.btnSoal.setText(listSoal.get(position));
        holder.btnSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity.getApplicationContext(), ActivitySoal3.class);
                intent.putExtra("posisi", position);
                intent.putExtra("jenis", jenis);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listSoal.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        Button btnSoal;

        public ViewHolder(View itemView) {
            super(itemView);

            btnSoal = (Button) itemView.findViewById(R.id.btn_soal);
        }
    }
}
