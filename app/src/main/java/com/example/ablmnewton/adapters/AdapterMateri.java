package com.example.ablmnewton.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.materi.ActivityMateri3;

public class AdapterMateri extends RecyclerView.Adapter<AdapterMateri.ViewHolder> {

    private Activity activity;
    private String [] arrJudulMateri;

    public AdapterMateri(Activity activity, String [] arrJudulMateri)
    {
        this.activity = activity;
        this.arrJudulMateri = arrJudulMateri;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_materi, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.btnMateri.setText(arrJudulMateri[position]);
        holder.btnMateri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity.getApplicationContext(), ActivityMateri3.class);
                intent.putExtra("posisi", position);
                intent.putExtra("materi", arrJudulMateri[position]);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrJudulMateri.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        Button btnMateri;

        public ViewHolder(View itemView) {
            super(itemView);

            btnMateri = (Button) itemView.findViewById(R.id.btn_materi);
        }
    }
}
