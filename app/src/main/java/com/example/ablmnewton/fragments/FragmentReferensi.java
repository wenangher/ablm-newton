package com.example.ablmnewton.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterReferensi;

public class FragmentReferensi extends Fragment {

    private View view;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;

    private String [] arrReferensi;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.layout_recycler, container, false);

        arrReferensi = getActivity().getResources().getStringArray(R.array.referensi);

        recycler = view.findViewById(R.id.recycler);
        RelativeLayout layout = view.findViewById(R.id.lay_utama);
        layout.setBackgroundColor(view.getContext().getResources().getColor(R.color.white));

        layoutManager = new LinearLayoutManager(view.getContext());

        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recycler.setAdapter(new AdapterReferensi(arrReferensi));

        return view;
    }
}
