package com.example.ablmnewton.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ablmnewton.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class FragmentPetaKonsep extends Fragment {

    View view;
    PDFView pdfView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.frag_pdf, container, false);
        pdfView = (PDFView) view.findViewById(R.id.pdfviewer);

        displayFromAsset(pdfView, "05_peta_konsep.pdf");

        return view;
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }

}
