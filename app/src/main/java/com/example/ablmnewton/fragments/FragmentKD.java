package com.example.ablmnewton.fragments;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ablmnewton.R;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

public class FragmentKD extends Fragment {

    View view;
    TableLayout table;
    JustifiedTextView tvKompetensi;
    String [] arrTxtKompetensi;
    int margin;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.frag_kompetensi, container, false);

        table = (TableLayout) view.findViewById(R.id.tb_kompetensi);
        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams();
        margin = (int) getResources().getDimension(R.dimen.dimen_margin);
        tableParams.bottomMargin = margin;

        arrTxtKompetensi = getResources().getStringArray(R.array.kompetensi_dasar);

        for(int i=0;i<arrTxtKompetensi.length;i++)
        {
            TableRow row = new TableRow(FragmentKD.this.getContext());

            TextView col1 = new TextView(FragmentKD.this.getContext());
            col1.setText("KD " + (2 + i + 1) + ".7");
            col1.setTextColor(getResources().getColor(R.color.black));

            tvKompetensi = new JustifiedTextView(FragmentKD.this.getContext());
            tvKompetensi.setTextColor(getResources().getColor(R.color.black));
            tvKompetensi.setText(arrTxtKompetensi[i]);

            TableRow.LayoutParams cp1 = new TableRow.LayoutParams();
            cp1.gravity = Gravity.TOP;
            cp1.span = 1;

            TableRow.LayoutParams cp2 = new TableRow.LayoutParams();
            cp2.gravity = Gravity.TOP;
            cp2.span = 24;

            col1.setLayoutParams(cp1);
            tvKompetensi.setLayoutParams(cp2);

            row.setLayoutParams(tableParams);

            row.addView(col1);
            row.addView(tvKompetensi);
            table.addView(row);
        }
        return view;
    }
}