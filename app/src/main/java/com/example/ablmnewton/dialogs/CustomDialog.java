package com.example.ablmnewton.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.evaluasi.ActivityPenjelasanEvaluasi;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

public class CustomDialog {

    Dialog dialog;
    VideoView video;
    private String isi;
    private JustifiedTextView tvPenjelasan;
    private Button btnOk, btnPenjelasan;
    private Context context;
    private PDFView pdfView;
    private TextView tvNilai;

    public CustomDialog(Context context, String isi)
    {
        this.context = context;
        dialog = new Dialog(context);
        this.isi = isi;
    }

    public void dialogRealPBL()
    {
        dialog.setContentView(R.layout.dialog_videopbl);

        tvPenjelasan = (JustifiedTextView) dialog.findViewById(R.id.tv_penjelasan_video);
        btnOk        = (Button) dialog.findViewById(R.id.btn_ok);

        tvPenjelasan.setText(isi);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void dialogVideo(int source, Activity activity)
    {
        dialog.setContentView(R.layout.dialog_showvideo);

        video = (VideoView) dialog.findViewById(R.id.video_realpbl);
        btnPenjelasan = (Button) dialog.findViewById(R.id.btn_penjelasan);

        String videoPath = "android.resource://"+activity.getPackageName()+"/"+source;

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0;

        dialog.getWindow().setAttributes(lp);

        Uri uri = Uri.parse(videoPath);
        video.setVideoURI(uri);

        MediaController mediaController = new MediaController(activity);
        video.setMediaController(mediaController);
        mediaController.setAnchorView(video);

        dialog.show();

        if (dialog.isShowing())
        {
            video.start();
        }

        btnPenjelasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                CustomDialog.this.dialogRealPBL();
            }
        });
    }

    public void dialogPenyelesaian()
    {
        dialog.setContentView(R.layout.dialog_penyelesaian);

        pdfView = dialog.findViewById(R.id.pdfviewer);
        btnOk   = (Button) dialog.findViewById(R.id.btn_ok);

        displayFromAsset(pdfView, isi);

        dialog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void dialogNilai(final Activity activity, int score)
    {
        dialog.setContentView(R.layout.dialog_nilai);

        tvNilai = dialog.findViewById(R.id.tv_nilai);
        btnPenjelasan = dialog.findViewById(R.id.btn_penjelasan);

        tvNilai.setText("" + score);

        dialog.show();

        btnPenjelasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(context, ActivityPenjelasanEvaluasi.class);
                intent.putExtra("evaluasi", isi);
                activity.startActivity(intent);
                activity.finish();
            }
        });
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }
}
