package com.example.ablmnewton.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.victor.loading.rotate.RotateLoading;

public class ActivitySplash extends AppCompatActivity {

    RotateLoading loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_splash);

        loading = (RotateLoading) findViewById(R.id.loading);
        loading.start();

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {

                loading.stop();

                Intent i = new Intent(ActivitySplash.this, ActivityBeranda.class);
                startActivity(i);

                finish();
            }
        }, 3000);
    }
}
