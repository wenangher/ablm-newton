package com.example.ablmnewton.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterTabs;
import com.example.ablmnewton.fragments.FragmentPetunjuk1;
import com.example.ablmnewton.fragments.FragmentPetunjuk2;
import com.google.android.material.tabs.TabLayout;

public class ActivityPetunjuk extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_viewpager);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getSupportActionBar().setElevation(0);
        }
        getSupportActionBar().setCustomView(R.layout.action_default);

        View view = getSupportActionBar().getCustomView();

        TextView txtMenu = (TextView) view.findViewById(R.id.tv_title);
        txtMenu.setText(ActivityPetunjuk.this.getTitle());

        tabLayout = (TabLayout) findViewById(R.id.tab_judul);
        viewPager = (ViewPager) findViewById(R.id.pager_isi);

        setupViewPager(viewPager);

        tabLayout.setBackgroundColor(getResources().getColor(R.color.cyan_500));
        tabLayout.setTabTextColors(getResources().getColor(R.color.cyan_100), getResources().getColor(R.color.white_text));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.cyan_700));
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        AdapterTabs adapter = new AdapterTabs(getSupportFragmentManager(),0);
        adapter.addFragment(new FragmentPetunjuk1(), "Petunjuk Penggunaan ABLM");
        adapter.addFragment(new FragmentPetunjuk2(), "Petunjuk Belajar");
        viewPager.setAdapter(adapter);
    }
}
