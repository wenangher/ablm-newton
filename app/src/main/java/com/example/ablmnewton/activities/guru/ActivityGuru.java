package com.example.ablmnewton.activities.guru;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.ActivityKontenPBL;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.KegiatanPBLActivity;
import com.example.ablmnewton.activities.pbl.ujikritis.UjiKritisActivity;

public class ActivityGuru extends AppCompatActivity {

    Button btnHasil1, btnHasil2, btnHasil3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_guru1);

        btnHasil1 = (Button) findViewById(R.id.btn_hasil_1);
        btnHasil2 = (Button) findViewById(R.id.btn_hasil_2);
        btnHasil3 = (Button) findViewById(R.id.btn_hasil_3);

        btnHasil1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityGuru.this, ActivityHasilEvaluasi.class);
                startActivity(intent);
            }
        });

        btnHasil2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityGuru.this, KegiatanPBLActivity.class);
                intent.putExtra(KegiatanPBLActivity.USER_TYPE,"GURU");
                startActivity(intent);

            }
        });

        btnHasil3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityGuru.this, UjiKritisActivity.class);
                intent.putExtra(KegiatanPBLActivity.USER_TYPE,"GURU");
                startActivity(intent);
            }
        });
    }
}
