package com.example.ablmnewton.activities.pbl.kegiatanpbl;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.PBLDuaFaseDuaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.PBLDuaFaseEmpatActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.PBLDuaFaseLimaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.PBLDuaFaseSatuActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.PBLDuaFaseTigaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseDuaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseEmpatActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseLimaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseSatuActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseTigaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.PBLTigaFaseDuaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.PBLTigaFaseEmpatActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.PBLTigaFaseLimaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.PBLTigaFaseSatuActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.PBLTigaFaseTigaActivity;
import com.example.ablmnewton.adapters.AdapterHasil;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EvaluasiPBLActivity extends AppCompatActivity {

    public static String USER_TYPE = "USER_TYPE";
    public static String FASE = "FASE";
    public static String KEGIATAN = "KEGIATAN";
    private AdapterHasil adapterHasil;
    private List<DataHasil> dataHasils = new ArrayList<>();
    private int fase,kegiatan;
    private String aktivitas;
    private DatabaseReference mDatabase;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi_pbl);

        fase = getIntent().getIntExtra(FASE,0);
        kegiatan = getIntent().getIntExtra(KEGIATAN,0);
        RecyclerView rvEvaluasi = findViewById(R.id.recyclerEvaluasi);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Kegiatan PBL "+kegiatan);
        adapterHasil = new AdapterHasil(dataHasils);
        rvEvaluasi.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rvEvaluasi.setAdapter(adapterHasil);

        mDatabase = FirebaseDatabase.getInstance().getReference("KegiatanPBL");

        if(kegiatan == 1){
            aktivitas = "PBLSatu";
        }else if(kegiatan == 2){
            aktivitas = "PBLDua";
        }else if(kegiatan == 3){
            aktivitas = "PBLTiga";
        }

        if(fase == 1){
            requestData(aktivitas,"FaseSatu");
        }else if(fase == 2){
            requestData(aktivitas,"FaseDua");
        }else if(fase == 3){
            requestData(aktivitas,"FaseTiga");
        }else if(fase == 4){
            requestData(aktivitas,"FaseEmpat");
        }else if(fase == 5){
            requestData(aktivitas,"FaseLima");
        }

        adapterHasil.OnItemClick(new AdapterHasil.onItemClick() {
            @Override
            public void onItemClicked(DataHasil dataHasil) {
                if(kegiatan == 1){
                    if(fase == 1){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLSatuFaseSatuActivity.class);
                        intent.putExtra(PBLSatuFaseSatuActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLSatuFaseSatuActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 2){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLSatuFaseDuaActivity.class);
                        intent.putExtra(PBLSatuFaseDuaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLSatuFaseDuaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 3){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLSatuFaseTigaActivity.class);
                        intent.putExtra(PBLSatuFaseTigaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLSatuFaseTigaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 4){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLSatuFaseEmpatActivity.class);
                        intent.putExtra(PBLSatuFaseEmpatActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLSatuFaseEmpatActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 5){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLSatuFaseLimaActivity.class);
                        intent.putExtra(PBLSatuFaseLimaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLSatuFaseLimaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }
                }else if(kegiatan == 2){
                    if(fase == 1){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLDuaFaseSatuActivity.class);
                        intent.putExtra(PBLDuaFaseSatuActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLDuaFaseSatuActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 2){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLDuaFaseDuaActivity.class);
                        intent.putExtra(PBLDuaFaseDuaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLDuaFaseDuaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 3){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLDuaFaseTigaActivity.class);
                        intent.putExtra(PBLDuaFaseTigaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLDuaFaseTigaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 4){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLDuaFaseEmpatActivity.class);
                        intent.putExtra(PBLDuaFaseEmpatActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLDuaFaseEmpatActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 5){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLDuaFaseLimaActivity.class);
                        intent.putExtra(PBLDuaFaseLimaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLDuaFaseLimaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }
                }else if(kegiatan == 3){
                    if(fase == 1){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLTigaFaseSatuActivity.class);
                        intent.putExtra(PBLTigaFaseSatuActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLTigaFaseSatuActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 2){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLTigaFaseDuaActivity.class);
                        intent.putExtra(PBLTigaFaseDuaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLTigaFaseDuaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 3){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLTigaFaseTigaActivity.class);
                        intent.putExtra(PBLTigaFaseTigaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLTigaFaseTigaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 4){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLTigaFaseEmpatActivity.class);
                        intent.putExtra(PBLTigaFaseEmpatActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLTigaFaseEmpatActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }else if(fase == 5){
                        Intent intent = new Intent(EvaluasiPBLActivity.this, PBLTigaFaseLimaActivity.class);
                        intent.putExtra(PBLTigaFaseLimaActivity.EXTRA_DATA,dataHasil);
                        intent.putExtra(PBLTigaFaseLimaActivity.USER_TYPE,"GURU");
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void requestData(String kegiatan,String fase){
        Utils.showDialog(EvaluasiPBLActivity.this);
        mDatabase.child(kegiatan).child(fase).addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot x : dataSnapshot.getChildren()){
                        DataHasil dataHasil = new DataHasil();
                        dataHasil.setId(x.getKey());
                        dataHasil.setNama(Objects.requireNonNull(x.child("user").getValue()).toString());
                        dataHasil.setNilai(Objects.requireNonNull(x.child("nilai").getValue()).toString());
                        dataHasil.setAnwswer1(x.child("answer1").getValue() == null?"": Objects.requireNonNull(x.child("answer1").getValue()).toString());
                        dataHasil.setAnwswer2(x.child("answer2").getValue() == null?"": Objects.requireNonNull(x.child("answer2").getValue()).toString());
                        dataHasil.setAnwswer3(x.child("answer3").getValue() == null?"": Objects.requireNonNull(x.child("answer3").getValue()).toString());
                        dataHasils.add(dataHasil);
                    }
                    adapterHasil.notifyDataSetChanged();
                    Utils.hideDialog();
                }else{
                    Toast.makeText(getBaseContext(), "Data Kosong", Toast.LENGTH_SHORT).show();
                    Utils.hideDialog();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }
}
