package com.example.ablmnewton.activities.materi;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterMateri;

public class ActivityMateri2 extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private String [] arrJudulMateri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_recycler2);

        arrJudulMateri = new String[]{"Hukum I Newton", "Hukum II Newton",
                                      "Hukum III Newton", "Penerapan Hukum Newton"};

        recycler = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(ActivityMateri2.this);

        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(new AdapterMateri(ActivityMateri2.this, arrJudulMateri));
    }
}
