package com.example.ablmnewton.activities.pbl;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;

public class ActivityVideoRealPBL extends AppCompatActivity {

    VideoView video;
    Button btnPenjelasan;
    CustomDialog dialog;

    int position;

    String [] arrPenjelasan;
    int [] arrRawVideo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_videorealpbl);

        video = (VideoView) findViewById(R.id.video_realpbl);
        btnPenjelasan = (Button) findViewById(R.id.btn_penjelasan);

        position = getIntent().getIntExtra("videopbl", 0);

        arrPenjelasan = getResources().getStringArray(R.array.arr_penjelasan_video);
        arrRawVideo = new int[]{R.raw.perahu, R.raw.belanja, R.raw.lift,
                                R.raw.memanah, R.raw.menjatuhkan_benda};

        dialog = new CustomDialog(ActivityVideoRealPBL.this, arrPenjelasan[position]);
        playVideo(arrRawVideo[position], ActivityVideoRealPBL.this);

        btnPenjelasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dialogRealPBL();
            }
        });
    }

    private void playVideo(int source, Context context){
        String videoPath = "android.resource://"+getPackageName()+"/"+source;

        Uri uri = Uri.parse(videoPath);
        video.setVideoURI(uri);

        MediaController mediaController = new MediaController(context);
        video.setMediaController(mediaController);
        mediaController.setAnchorView(video);
        video.start();
    }
}
