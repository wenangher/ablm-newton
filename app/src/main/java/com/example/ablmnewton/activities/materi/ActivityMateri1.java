package com.example.ablmnewton.activities.materi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;

public class ActivityMateri1 extends AppCompatActivity {

    Button btnMateri, btnContohSoal, btnPBL;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_materi1);

        btnMateri = (Button) findViewById(R.id.btn_materi);
        btnContohSoal = (Button) findViewById(R.id.btn_contoh_soal);


        btnMateri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityMateri1.this, ActivityMateri2.class);
                startActivity(intent);
            }
        });

        btnContohSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityMateri1.this, ActivitySoal1.class);
                startActivity(intent);
            }
        });
    }
}
