package com.example.ablmnewton.activities.materi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;

public class ActivitySoal1 extends AppCompatActivity {

    Button btnSoal1, btnSoal2, btnSoal3;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_soal1);

        btnSoal1 = (Button) findViewById(R.id.btn_soal1);
        btnSoal2 = (Button) findViewById(R.id.btn_soal2);
        btnSoal3 = (Button) findViewById(R.id.btn_soal3);

        btnSoal1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivitySoal1.this, ActivitySoal2.class);
                intent.putExtra("posisi", 0);
                intent.putExtra("judul", "Contoh Soal Hukum I Newton");
                startActivity(intent);
            }
        });

        btnSoal2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivitySoal1.this, ActivitySoal2.class);
                intent.putExtra("posisi", 1);
                intent.putExtra("judul", "Contoh Soal Hukum II Newton");
                startActivity(intent);
            }
        });

        btnSoal3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivitySoal1.this, ActivitySoal2.class);
                intent.putExtra("posisi", 2);
                intent.putExtra("judul", "Contoh Soal Hukum III Newton dan Penerapan");
                startActivity(intent);
            }
        });
    }
}
