package com.example.ablmnewton.activities.pbl;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterRealPBL;
import com.example.ablmnewton.data.DataRealPBL;

import java.util.ArrayList;
import java.util.List;

public class ActivityRealPBL extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    List<DataRealPBL> listData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_recycler);

        int [] imagePBL =  {R.drawable.prahu, R.drawable.belanja, R.drawable.lift,
                            R.drawable.memanah, R.drawable.menjatuhkan_benda};

        String [] judulPBL = {"Perahu", "Belanja", "Lift", "Memanah", "Menjatuhkan Benda"};

        listData = new ArrayList<>();

        for (int i=0; i<imagePBL.length; i++)
        {
            DataRealPBL dataPBL = new DataRealPBL(judulPBL[i], imagePBL[i]);
            listData.add(dataPBL);
        }

        recycler = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(ActivityRealPBL.this);

        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(new AdapterRealPBL(ActivityRealPBL.this, listData));
    }
}
