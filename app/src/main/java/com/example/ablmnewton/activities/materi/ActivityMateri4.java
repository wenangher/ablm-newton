package com.example.ablmnewton.activities.materi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;

public class ActivityMateri4 extends AppCompatActivity {

    Button btnPenerapan1, btnPenerapan2;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_materi4);

        btnPenerapan1 = (Button) findViewById(R.id.btn_penerapan1);
        btnPenerapan2 = (Button) findViewById(R.id.btn_penerapan2);

        btnPenerapan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(ActivityMateri4.this, ActivityMateri5.class);
                intent.putExtra("penerapan", 0);
                startActivity(intent);
            }
        });

        btnPenerapan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(ActivityMateri4.this, ActivityMateri5.class);
                intent.putExtra("penerapan", 1);
                startActivity(intent);
            }
        });
    }
}
