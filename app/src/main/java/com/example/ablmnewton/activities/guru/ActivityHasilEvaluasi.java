package com.example.ablmnewton.activities.guru;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterHasilEvaluasi;
import com.example.ablmnewton.data.DataHasilEvaluasi;
import com.example.ablmnewton.interfaces.InterfaceResultRequest;
import com.example.ablmnewton.requests.GetEvaluasiAll;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityHasilEvaluasi extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    List<DataHasilEvaluasi> listData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_recycler);

        recycler = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(ActivityHasilEvaluasi.this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    public void loadData()
    {
        new GetEvaluasiAll(ActivityHasilEvaluasi.this, ActivityHasilEvaluasi.this, new InterfaceResultRequest() {
            @Override
            public void getData(JSONObject jsonData) {
                try {
                    listData = new ArrayList<>();

                    JSONArray jsonArray = jsonData.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject detail = jsonArray.getJSONObject(i);

                        String nama = detail.getString("nama");
                        String nis = detail.getString("nis");
                        int nilai1 = detail.getInt("nilai_1");
                        int nilai2 = detail.getInt("nilai_2");
                        int nilai3 = detail.getInt("nilai_3");

                        DataHasilEvaluasi dataEvaluasi = new DataHasilEvaluasi(nama, nis, nilai1, nilai2, nilai3);
                        listData.add(dataEvaluasi);
                    }

                    recycler.setLayoutManager(layoutManager);
                    recycler.setItemAnimator(new DefaultItemAnimator());
                    recycler.setAdapter(new AdapterHasilEvaluasi(ActivityHasilEvaluasi.this, listData));

                } catch (final JSONException e) {
                    ActivityHasilEvaluasi.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivityHasilEvaluasi.this,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
