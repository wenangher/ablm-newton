package com.example.ablmnewton.activities.evaluasi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;
import com.example.ablmnewton.interfaces.InterfaceCheckIfValid;
import com.example.ablmnewton.requests.UpdateEvaluasi3;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityEvaluasi23 extends AppCompatActivity {

    PDFView pdf1, pdf2, pdf3, pdf4, pdf5;
    RadioButton rbPil1a, rbPil1b, rbPil1c, rbPil1d, rbPil1e, rbPil2a, rbPil2b, rbPil2c, rbPil2d,
            rbPil2e, rbPil3a, rbPil3b, rbPil3c, rbPil3d, rbPil3e, rbPil4a, rbPil4b, rbPil4c,
            rbPil4d, rbPil4e, rbPil5a, rbPil5b, rbPil5c, rbPil5d, rbPil5e;
    Button btnSubmit;

    int score;
    private String nama, nis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_evaluasi_2_3);

        nama    = getIntent().getStringExtra("nama");
        nis     = getIntent().getStringExtra("nis");

        pdf1 = (PDFView) findViewById(R.id.pdf1);
        pdf2 = (PDFView) findViewById(R.id.pdf2);
        pdf3 = (PDFView) findViewById(R.id.pdf3);
        pdf4 = (PDFView) findViewById(R.id.pdf4);
        pdf5 = (PDFView) findViewById(R.id.pdf5);

        rbPil1a = findViewById(R.id.rb_pil_1_a);
        rbPil1b = findViewById(R.id.rb_pil_1_b);
        rbPil1c = findViewById(R.id.rb_pil_1_c);
        rbPil1d = findViewById(R.id.rb_pil_1_d);
        rbPil1e = findViewById(R.id.rb_pil_1_e);

        rbPil2a = findViewById(R.id.rb_pil_2_a);
        rbPil2b = findViewById(R.id.rb_pil_2_b);
        rbPil2c = findViewById(R.id.rb_pil_2_c);
        rbPil2d = findViewById(R.id.rb_pil_2_d);
        rbPil2e = findViewById(R.id.rb_pil_2_e);

        rbPil3a = findViewById(R.id.rb_pil_3_a);
        rbPil3b = findViewById(R.id.rb_pil_3_b);
        rbPil3c = findViewById(R.id.rb_pil_3_c);
        rbPil3d = findViewById(R.id.rb_pil_3_d);
        rbPil3e = findViewById(R.id.rb_pil_3_e);

        rbPil4a = findViewById(R.id.rb_pil_4_a);
        rbPil4b = findViewById(R.id.rb_pil_4_b);
        rbPil4c = findViewById(R.id.rb_pil_4_c);
        rbPil4d = findViewById(R.id.rb_pil_4_d);
        rbPil4e = findViewById(R.id.rb_pil_4_e);

        rbPil5a = findViewById(R.id.rb_pil_5_a);
        rbPil5b = findViewById(R.id.rb_pil_5_b);
        rbPil5c = findViewById(R.id.rb_pil_5_c);
        rbPil5d = findViewById(R.id.rb_pil_5_d);
        rbPil5e = findViewById(R.id.rb_pil_5_e);

        btnSubmit = (Button) findViewById(R.id.btn_submit);

        dataSoal();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSoal();

                score = nilai();

                kirimNilai(nama, score);
            }
        });
    }

    private  void dataSoal(){
        pdf1.fromAsset("Latihan 3_1.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf2.fromAsset("Latihan 3_2.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf3.fromAsset("Latihan 3_3.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf4.fromAsset("Latihan 3_4.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf5.fromAsset("Latihan 3_5.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        dataPilihan();
    }

    private void dataPilihan(){
        rbPil1a.setText("A. Arahnya sama dengan arah F");
        rbPil1b.setText("B. Tidak terletak pada satu garis gaya");
        rbPil1c.setText("C. Besarnya sama dengan Fdengan arah yang berlawanan");
        rbPil1d.setText("D. Arahnya membentuk sudut 90°");
        rbPil1e.setText("E. Tidak dapat ditentukan");

        rbPil2a.setText("A. 200 N");
        rbPil2b.setText("B. -200 N");
        rbPil2c.setText("C. 400 N");
        rbPil2d.setText("D. -400 N");
        rbPil2e.setText("E. 100 N");

        rbPil3a.setText("A. F dan F1");
        rbPil3b.setText("B. F1 dan F4");
        rbPil3c.setText("C. F2 dan F3");
        rbPil3d.setText("D. F3 dan F4");
        rbPil3e.setText("E. F dan F4");

        rbPil4a.setText("A. 2 m/s2 dan 6000 N");
        rbPil4b.setText("B. 2 m/s2 dan 9000 N");
        rbPil4c.setText("C. 3 m/s2 dan 9000 N");
        rbPil4d.setText("D. 4 m/s2 dan 10000 N");
        rbPil4e.setText("E. 5 m/s2 dan 10000 N");

        rbPil5a.setText("A. 1,29 kg");
        rbPil5b.setText("B. 1,30 kg");
        rbPil5c.setText("C. 1,33 kg");
        rbPil5d.setText("D. 1,35 kg");
        rbPil5e.setText("E. 1,36 kg ");
    }

    private int nilai (){
        int nilai =0;
        if(rbPil1c.isChecked()) nilai++;
        if(rbPil2b.isChecked()) nilai++;
        if(rbPil3d.isChecked()) nilai++;
        if(rbPil4b.isChecked()) nilai++;
        if(rbPil5c.isChecked()) nilai++;

        score = nilai * 20;

        return score;
    }

    private void kirimNilai(String nama, final int score)
    {
        String[] body = new String[2];
        body[0] = nama;
        body[1] = "" + score;

        new UpdateEvaluasi3(ActivityEvaluasi23.this, ActivityEvaluasi23.this, body, new InterfaceCheckIfValid() {
            @Override
            public void checkForRequest(Boolean condition) {
                if (condition)
                {
                    Toast.makeText(ActivityEvaluasi23.this, "Sukses menambahkan data.", Toast.LENGTH_SHORT).show();
                    CustomDialog dialog = new CustomDialog(ActivityEvaluasi23.this, "eva3");
                    dialog.dialogNilai(ActivityEvaluasi23.this, score);
                }
                else
                {
                    Toast.makeText(ActivityEvaluasi23.this,
                            "Gagal menambahkan data.\nSilahkan coba lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
