package com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class PBLDuaFaseEmpatActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    public static String USER_TYPE = "USER_TYPE";
    public static String EXTRA_DATA = "EXTRA_DATA";
    public static String USER = "USER";
    private EditText inputAnswer1, inputAnswer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbldua_fase_empat);

        String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        mDatabase = FirebaseDatabase.getInstance().getReference("KegiatanPBL");

        PDFView pdf = findViewById(R.id.pdf);
        PDFView pdf2 = findViewById(R.id.pdf2);
        inputAnswer1 = findViewById(R.id.answer1);
        inputAnswer2 = findViewById(R.id.answer2);
        Button submit = findViewById(R.id.btn_submit);

        pdf.fromAsset("PBL - 24a.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf2.fromAsset("PBL - 24b.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        assert type != null;
        if (type.equals("GURU")) {
            DataHasil dataHasil = getIntent().getParcelableExtra(EXTRA_DATA);
            submit.setVisibility(View.INVISIBLE);
            inputAnswer1.setEnabled(false);
            inputAnswer2.setEnabled(false);
            inputAnswer1.setTextColor(getResources().getColor(R.color.black));
            inputAnswer2.setTextColor(getResources().getColor(R.color.black));
            assert dataHasil != null;
            inputAnswer1.setText(dataHasil.getAnwswer1());
            inputAnswer2.setText(dataHasil.getAnwswer1());
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String answer1 = inputAnswer1.getText().toString();
                String answer2 = inputAnswer2.getText().toString();
                if (answer1.equals(" ") || answer1.isEmpty() || answer2.equals(" ") || answer2.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Anda belum menjawab", Toast.LENGTH_SHORT).show();
                } else {
                    assert user != null;
                    sendAnswer(user, answer1, answer2);
                }
            }
        });
    }

    private void sendAnswer(String users, final String answer1, final String answer2) {
        Utils.showDialog(PBLDuaFaseEmpatActivity.this);
        HashMap<String, String> maps = new HashMap<>();
        maps.put("user", users);
        maps.put("answer1", answer1);
        maps.put("answer2", answer2);
        maps.put("status", "0");
        maps.put("nilai", "");
        mDatabase.child("PBLDua").child("FaseEmpat").push().setValue(maps).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getBaseContext(), "Berhasil mengirim data", Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }
}
