package com.example.ablmnewton.activities.pbl.ujikritis;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterHasil;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EvaluasiUjiKritisActivity extends AppCompatActivity {

    public static String KEGIATAN = "KEGIATAN";
    public static String USER_TYPE = "USER_TYPE";
    private AdapterHasil adapterHasil;
    private List<DataHasil> dataHasils = new ArrayList<>();
    private int kegiatan;
    private DatabaseReference mDatabase;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi_uji_kritis);

        kegiatan = getIntent().getIntExtra(KEGIATAN, 0);
        RecyclerView rvEvaluasi = findViewById(R.id.recyclerEvaluasi);

        Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Uji Kritis " + kegiatan);
        adapterHasil = new AdapterHasil(dataHasils);
        rvEvaluasi.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvEvaluasi.setAdapter(adapterHasil);

        mDatabase = FirebaseDatabase.getInstance().getReference("UjiKritis");

        String aktivitas;
        if (kegiatan == 1) {
            aktivitas = "UjiKritisSatu";
            requestData(aktivitas);
        } else if (kegiatan == 2) {
            aktivitas = "UjiKritisDua";
            requestData(aktivitas);
        } else if (kegiatan == 3) {
            aktivitas = "UjiKritisTiga";
            requestData(aktivitas);
        }

        adapterHasil.OnItemClick(new AdapterHasil.onItemClick() {
            @Override
            public void onItemClicked(DataHasil dataHasil) {
                if (kegiatan == 1) {
                    Intent intent = new Intent(EvaluasiUjiKritisActivity.this, UjiKritisSatuActivity.class);
                    intent.putExtra(UjiKritisSatuActivity.EXTRA_DATA, dataHasil);
                    intent.putExtra(UjiKritisSatuActivity.USER_TYPE, "GURU");
                    startActivity(intent);
                } else if (kegiatan == 2) {
                    Intent intent = new Intent(EvaluasiUjiKritisActivity.this, UjiKritisDuaActivity.class);
                    intent.putExtra(UjiKritisDuaActivity.EXTRA_DATA, dataHasil);
                    intent.putExtra(UjiKritisDuaActivity.USER_TYPE, "GURU");
                    startActivity(intent);
                } else if (kegiatan == 3) {
                    Intent intent = new Intent(EvaluasiUjiKritisActivity.this, UjiKritisTigaActivity.class);
                    intent.putExtra(UjiKritisTigaActivity.EXTRA_DATA, dataHasil);
                    intent.putExtra(UjiKritisTigaActivity.USER_TYPE, "GURU");
                    startActivity(intent);
                }
            }
        });

    }

    private void requestData(String kegiatan) {
        Utils.showDialog(EvaluasiUjiKritisActivity.this);
        mDatabase.child(kegiatan).addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot x : dataSnapshot.getChildren()) {
                        DataHasil dataHasil = new DataHasil();
                        dataHasil.setId(x.getKey());
                        dataHasil.setNama(Objects.requireNonNull(x.child("user").getValue()).toString());
                        dataHasil.setNilai(Objects.requireNonNull(x.child("nilai").getValue()).toString());
                        dataHasil.setAnwswer1(x.child("answer1").getValue() == null ? "" : Objects.requireNonNull(x.child("answer1").getValue()).toString());
                        dataHasil.setAnwswer2(x.child("answer2").getValue() == null ? "" : Objects.requireNonNull(x.child("answer2").getValue()).toString());
                        dataHasil.setAnwswer3(x.child("answer3").getValue() == null ? "" : Objects.requireNonNull(x.child("answer3").getValue()).toString());
                        dataHasils.add(dataHasil);
                    }
                    adapterHasil.notifyDataSetChanged();
                    Utils.hideDialog();
                } else {
                    Toast.makeText(getBaseContext(), "Data Kosong", Toast.LENGTH_SHORT).show();
                    Utils.hideDialog();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getBaseContext(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }
}
