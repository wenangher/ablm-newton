package com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.EvaluasiPBLActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.PBLSatuFaseLimaActivity;

import java.util.Objects;

public class KegiatanPBLDuaActivity extends AppCompatActivity {

    public static String USER_TYPE = "USER_TYPE";
    public static String USER = "USER";


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_pbldua);

        final String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        Button fase1 = findViewById(R.id.btn_fase_1);
        Button fase2 = findViewById(R.id.btn_fase_2);
        Button fase3 = findViewById(R.id.btn_fase_3);
        Button fase4 = findViewById(R.id.btn_fase_4);
        Button fase5 = findViewById(R.id.btn_fase_5);

        assert type != null;
        if(type.equals("GURU")){
            Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Kegiatan PBL 2");
        }

        fase1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,1);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,2);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, PBLDuaFaseSatuActivity.class);
                    intent.putExtra(PBLDuaFaseSatuActivity.USER_TYPE,type);
                    intent.putExtra(PBLDuaFaseSatuActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,2);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,2);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, PBLDuaFaseDuaActivity.class);
                    intent.putExtra(PBLDuaFaseDuaActivity.USER_TYPE,type);
                    intent.putExtra(PBLDuaFaseDuaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,3);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,2);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, PBLDuaFaseTigaActivity.class);
                    intent.putExtra(PBLDuaFaseTigaActivity.USER_TYPE,type);
                    intent.putExtra(PBLDuaFaseTigaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,4);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,2);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, PBLDuaFaseEmpatActivity.class);
                    intent.putExtra(PBLDuaFaseEmpatActivity.USER_TYPE,type);
                    intent.putExtra(PBLDuaFaseEmpatActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,4);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,2);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLDuaActivity.this, PBLDuaFaseLimaActivity.class);
                    intent.putExtra(PBLDuaFaseLimaActivity.USER_TYPE,type);
                    intent.putExtra(PBLDuaFaseLimaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

    }

}
