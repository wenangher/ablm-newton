package com.example.ablmnewton.activities.evaluasi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;

public class ActivityEvaluasi1 extends AppCompatActivity {

    Button btnEva1, btnEva2, btnEva3;
    String nama, nis;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_evaluasi1);

        nama    = getIntent().getStringExtra("nama");
        nis     = getIntent().getStringExtra("nis");

        btnEva1 = (Button) findViewById(R.id.btn_eva1);
        btnEva2 = (Button) findViewById(R.id.btn_eva2);
        btnEva3 = (Button) findViewById(R.id.btn_eva3);

        btnEva1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(ActivityEvaluasi1.this, ActivityEvaluasi21.class);
                intent.putExtra("nama", nama);
                intent.putExtra("nis", nis);
                startActivity(intent);
            }
        });

        btnEva2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(ActivityEvaluasi1.this, ActivityEvaluasi22.class);
                intent.putExtra("nama", nama);
                intent.putExtra("nis", nis);
                startActivity(intent);
            }
        });

        btnEva3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(ActivityEvaluasi1.this, ActivityEvaluasi23.class);
                intent.putExtra("nama", nama);
                intent.putExtra("nis", nis);
                startActivity(intent);
            }
        });
    }
}
