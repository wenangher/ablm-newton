package com.example.ablmnewton.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.ActivityKontenPBL;

public class ActivityBeranda extends AppCompatActivity {

    ImageButton btnKompetensi,
                btnPetunjuk,
                btnKontenPBL,
                btnEvaluasi,
                btnPengembang;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_beranda);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_beranda);

        btnKompetensi   = (ImageButton) findViewById(R.id.btn_kompetensi);
        btnPetunjuk     = (ImageButton) findViewById(R.id.btn_petunjuk);
        btnKontenPBL    = (ImageButton) findViewById(R.id.btn_kontenpbl);
        btnEvaluasi     = (ImageButton) findViewById(R.id.btn_evaluasi);
        btnPengembang   = (ImageButton) findViewById(R.id.btn_pengembang);


        btnPetunjuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityBeranda.this, ActivityPetunjuk.class);
                startActivity(intent);
            }
        });

        btnKompetensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityBeranda.this, ActivityKompetensi.class);
                startActivity(intent);
            }
        });

        btnPengembang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityBeranda.this, ActivityPengembang.class);
                startActivity(intent);
            }
        });

        btnEvaluasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityBeranda.this, ActivityLogin.class);
                startActivity(intent);
            }
        });

        btnKontenPBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ActivityBeranda.this, ActivityKontenPBL.class);
                startActivity(intent);
            }
        });
    }
}
