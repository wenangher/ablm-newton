package com.example.ablmnewton.activities.materi;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityMateri5 extends AppCompatActivity {

    PDFView pdfView;

    int posisi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.frag_pdf);

        pdfView = (PDFView) findViewById(R.id.pdfviewer);

        posisi = getIntent().getIntExtra("penerapan", 0);

        String [] judul = {"Gerak Benda yang Dihubungkan", "Gerak Benda di Dalam Lift"};
        String [] arrPenerapan = {"Materi Penerapan 1.pdf", "Materi Penerapan 2.pdf"};

        this.setTitle(judul[posisi]);
        displayFromAsset(pdfView, arrPenerapan[posisi]);
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }
}
