package com.example.ablmnewton.activities.materi;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.adapters.AdapterSoal;

import java.util.ArrayList;
import java.util.List;

public class ActivitySoal2 extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private List<String> listSoal;

    private int posisi;
    private int jumlahSoal;

    private String jenisSoal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_recycler2);

        listSoal = new ArrayList<>();

        posisi = getIntent().getIntExtra("posisi", 0);

        this.setTitle(getIntent().getStringExtra("judul"));

        switch (posisi)
        {
            case 0:
                jumlahSoal = 5;
                jenisSoal = "a";
                break;
            case 1:
                jumlahSoal = 7;
                jenisSoal = "b";
                break;
            case 2:
                jumlahSoal = 7;
                jenisSoal = "c";
                break;
        }

        Log.d("Jml Soal", "" + jumlahSoal);

        for (int i=1; i<=jumlahSoal; i++)
        {
            listSoal.add("Contoh Soal " + i);
        }

        recycler = (RecyclerView) findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(ActivitySoal2.this);

        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(new AdapterSoal(ActivitySoal2.this, listSoal, jenisSoal));
    }
}
