package com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.ujikritis.UjiKritisActivity;
import com.example.ablmnewton.activities.pbl.ujikritis.UjiKritisDuaActivity;
import com.example.ablmnewton.activities.pbl.ujikritis.UjiKritisSatuActivity;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;

public class PBLSatuFaseSatuActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    public static String USER_TYPE = "USER_TYPE";
    public static String EXTRA_DATA = "EXTRA_DATA";
    public static String USER = "USER";
    private EditText inputAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pblsatu_fase_satu);

        String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        mDatabase = FirebaseDatabase.getInstance().getReference("KegiatanPBL");

        PDFView pdf = findViewById(R.id.pdf);
        inputAnswer = findViewById(R.id.answer1);
        Button submit = findViewById(R.id.btn_submit);

        pdf.fromAsset("PBL - 11.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playVideo();
            }
        });

        assert type != null;
        if (type.equals("GURU")) {
            DataHasil dataHasil = getIntent().getParcelableExtra(EXTRA_DATA);
            submit.setVisibility(View.INVISIBLE);
            inputAnswer.setEnabled(false);
            inputAnswer.setTextColor(getResources().getColor(R.color.black));
            assert dataHasil != null;
            inputAnswer.setText(dataHasil.getAnwswer1());
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String answer = inputAnswer.getText().toString();
                if (answer.equals(" ") || answer.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Anda belum menjawab", Toast.LENGTH_SHORT).show();
                } else {
                    assert user != null;
                    sendAnswer(user, answer);
                }
            }
        });
    }

    private void sendAnswer(String users, final String answer) {
        Utils.showDialog(PBLSatuFaseSatuActivity.this);
        HashMap<String, String> maps = new HashMap<>();
        maps.put("user", users);
        maps.put("answer1", answer);
        maps.put("status", "0");
        maps.put("nilai", "");
        mDatabase.child("PBLSatu").child("FaseSatu").push().setValue(maps).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getBaseContext(), "Berhasil mengirim data", Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void playVideo(){
        final Dialog dialog = new Dialog(PBLSatuFaseSatuActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_video);
        dialog.setCancelable(true);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        VideoView video = dialog.findViewById(R.id.video_pbl);
        String videoPath = "android.resource://"+getPackageName()+"/"+R.raw.mobil_jalan;

        Uri uri = Uri.parse(videoPath);
        video.setVideoURI(uri);

        MediaController mediaController = new MediaController(PBLSatuFaseSatuActivity.this);
        video.setMediaController(mediaController);
        mediaController.setAnchorView(video);
        video.start();

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
