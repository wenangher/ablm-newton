package com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.EvaluasiPBLActivity;

import java.util.Objects;

public class KegiatanPBLSatuActivity extends AppCompatActivity {

    public static String USER_TYPE = "USER_TYPE";
    public static String USER = "USER";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_pblsatu);

        final String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        assert type != null;
        if(type.equals("GURU")){
            Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Kegiatan PBL 1");
        }

        Button fase1 = findViewById(R.id.btn_fase_1);
        Button fase2 = findViewById(R.id.btn_fase_2);
        Button fase3 = findViewById(R.id.btn_fase_3);
        Button fase4 = findViewById(R.id.btn_fase_4);
        Button fase5 = findViewById(R.id.btn_fase_5);

        fase1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,1);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,1);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this,PBLSatuFaseSatuActivity.class);
                    intent.putExtra(KegiatanPBLSatuActivity.USER_TYPE,type);
                    intent.putExtra(KegiatanPBLSatuActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,2);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,1);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this,PBLSatuFaseDuaActivity.class);
                    intent.putExtra(PBLSatuFaseDuaActivity.USER_TYPE,type);
                    intent.putExtra(PBLSatuFaseDuaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,3);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,1);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this,PBLSatuFaseTigaActivity.class);
                    intent.putExtra(PBLSatuFaseTigaActivity.USER_TYPE,type);
                    intent.putExtra(PBLSatuFaseTigaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,4);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,1);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this,PBLSatuFaseEmpatActivity.class);
                    intent.putExtra(PBLSatuFaseEmpatActivity.USER_TYPE,type);
                    intent.putExtra(PBLSatuFaseEmpatActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

        fase5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this, EvaluasiPBLActivity.class);
                    intent.putExtra(EvaluasiPBLActivity.USER_TYPE,type);
                    intent.putExtra(EvaluasiPBLActivity.FASE,5);
                    intent.putExtra(EvaluasiPBLActivity.KEGIATAN,1);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(KegiatanPBLSatuActivity.this,PBLSatuFaseLimaActivity.class);
                    intent.putExtra(PBLSatuFaseLimaActivity.USER_TYPE,type);
                    intent.putExtra(PBLSatuFaseLimaActivity.USER,user);
                    startActivity(intent);
                }
            }
        });

    }
}
