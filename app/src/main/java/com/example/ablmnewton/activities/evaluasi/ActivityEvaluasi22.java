package com.example.ablmnewton.activities.evaluasi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;
import com.example.ablmnewton.interfaces.InterfaceCheckIfValid;
import com.example.ablmnewton.requests.UpdateEvaluasi2;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityEvaluasi22 extends AppCompatActivity {

    PDFView pdf1, pdf2, pdf3, pdf4, pdf5, pdf6, pdf7;
    RadioButton rbPil1a, rbPil1b, rbPil1c, rbPil1d, rbPil1e, rbPil2a, rbPil2b, rbPil2c, rbPil2d,
            rbPil2e, rbPil3a, rbPil3b, rbPil3c, rbPil3d, rbPil3e, rbPil4a, rbPil4b, rbPil4c,
            rbPil4d, rbPil4e, rbPil5a, rbPil5b, rbPil5c, rbPil5d, rbPil5e, rbPil6a, rbPil6b,
            rbPil6c, rbPil6d, rbPil6e, rbPil7a, rbPil7b, rbPil7c, rbPil7d, rbPil7e;
    Button btnSubmit;

    int score;
    private String nama, nis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_evaluasi_2_2);

        nama    = getIntent().getStringExtra("nama");
        nis     = getIntent().getStringExtra("nis");

        pdf1 = (PDFView) findViewById(R.id.pdf1);
        pdf2 = (PDFView) findViewById(R.id.pdf2);
        pdf3 = (PDFView) findViewById(R.id.pdf3);
        pdf4 = (PDFView) findViewById(R.id.pdf4);
        pdf5 = (PDFView) findViewById(R.id.pdf5);
        pdf6 = (PDFView) findViewById(R.id.pdf6);
        pdf7 = (PDFView) findViewById(R.id.pdf7);

        rbPil1a = findViewById(R.id.rb_pil_1_a);
        rbPil1b = findViewById(R.id.rb_pil_1_b);
        rbPil1c = findViewById(R.id.rb_pil_1_c);
        rbPil1d = findViewById(R.id.rb_pil_1_d);
        rbPil1e = findViewById(R.id.rb_pil_1_e);

        rbPil2a = findViewById(R.id.rb_pil_2_a);
        rbPil2b = findViewById(R.id.rb_pil_2_b);
        rbPil2c = findViewById(R.id.rb_pil_2_c);
        rbPil2d = findViewById(R.id.rb_pil_2_d);
        rbPil2e = findViewById(R.id.rb_pil_2_e);

        rbPil3a = findViewById(R.id.rb_pil_3_a);
        rbPil3b = findViewById(R.id.rb_pil_3_b);
        rbPil3c = findViewById(R.id.rb_pil_3_c);
        rbPil3d = findViewById(R.id.rb_pil_3_d);
        rbPil3e = findViewById(R.id.rb_pil_3_e);

        rbPil4a = findViewById(R.id.rb_pil_4_a);
        rbPil4b = findViewById(R.id.rb_pil_4_b);
        rbPil4c = findViewById(R.id.rb_pil_4_c);
        rbPil4d = findViewById(R.id.rb_pil_4_d);
        rbPil4e = findViewById(R.id.rb_pil_4_e);

        rbPil5a = findViewById(R.id.rb_pil_5_a);
        rbPil5b = findViewById(R.id.rb_pil_5_b);
        rbPil5c = findViewById(R.id.rb_pil_5_c);
        rbPil5d = findViewById(R.id.rb_pil_5_d);
        rbPil5e = findViewById(R.id.rb_pil_5_e);

        rbPil6a = findViewById(R.id.rb_pil_6_a);
        rbPil6b = findViewById(R.id.rb_pil_6_b);
        rbPil6c = findViewById(R.id.rb_pil_6_c);
        rbPil6d = findViewById(R.id.rb_pil_6_d);
        rbPil6e = findViewById(R.id.rb_pil_6_e);

        rbPil7a = findViewById(R.id.rb_pil_7_a);
        rbPil7b = findViewById(R.id.rb_pil_7_b);
        rbPil7c = findViewById(R.id.rb_pil_7_c);
        rbPil7d = findViewById(R.id.rb_pil_7_d);
        rbPil7e = findViewById(R.id.rb_pil_7_e);

        btnSubmit = (Button) findViewById(R.id.btn_submit);

        dataSoal();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSoal();

                score = nilai();

                kirimNilai(nama, score);
            }
        });
    }

    private  void dataSoal(){
        pdf1.fromAsset("Latihan 2_1.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf2.fromAsset("Latihan 2_2.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf3.fromAsset("Latihan 2_3.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf4.fromAsset("Latihan 2_4.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf5.fromAsset("Latihan 2_5.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf6.fromAsset("Latihan 2_6.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf7.fromAsset("Latihan 2_7.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        dataPilihan();
    }

    private void dataPilihan(){
        rbPil1a.setText("A. mengurangi massa yang ada di kereta bayi dan memilih jalan yang memiliki gesekan besar");
        rbPil1b.setText("B. mengurangi massa yang ada di kereta bayi dan memilih jalan yang memiliki gesekan kecil");
        rbPil1c.setText("C. menambah massa yang ada di kereta bayi dan memilih jalan yang memiliki gesekan besar");
        rbPil1d.setText("D. memberikan sabuk pengaman untuk bayi sehingga ibu bisa mendorong cepat");
        rbPil1e.setText("E. memberikan sabuk pengaman untuk bayi dan memilih jalan yang memiliki gesekan besar");

        rbPil2a.setText("A. fs > fk");
        rbPil2b.setText("B. fs < fk");
        rbPil2c.setText("C. fs = fk");
        rbPil2d.setText("D. fs = fk = 0");
        rbPil2e.setText("E. fs dan fk ≠ 0");

        rbPil3a.setText("A. A, D, E, C, B");
        rbPil3b.setText("B. B, A, E, C, D");
        rbPil3c.setText("C. C, D, A, E, B");
        rbPil3d.setText("D. D, C, E, A, B");
        rbPil3e.setText("E. E, C, D, B, A");

        rbPil4a.setText("A. a= 2 m/s^2 dan 12 m");
        rbPil4b.setText("B. a= 2 m/s^2 dan 36 m");
        rbPil4c.setText("C. a= 4 m/s^2 dan 12 m");
        rbPil4d.setText("D. a= 4 m/s^2 dan 36 m");
        rbPil4e.setText("E. a= 5 m/s^2 dan 16 m");

        rbPil5a.setText("A. m2 g");
        rbPil5b.setText("B. m1 g");
        rbPil5c.setText("C. (m1 + m2)g");
        rbPil5d.setText("D. (m1 - m2)g");
        rbPil5e.setText("E. m1 m2 g");

        rbPil6a.setText("A. Data salah karena gaya seharusnya tetap.");
        rbPil6b.setText("B. Data benar karena percepatan berubah.");
        rbPil6c.setText("C. Data benar karena massa seharusnya berubah.");
        rbPil6d.setText("D. Sata salah karena massa seharusnya berubah.");
        rbPil6e.setText("E. Data salah karena percepatan seharusnya tetap.");

        rbPil7a.setText("A. 0,3");
        rbPil7b.setText("B. 0,4");
        rbPil7c.setText("C. 0,5");
        rbPil7d.setText("D. 0,6");
        rbPil7e.setText("E. 0,7");
    }

    private int nilai (){
        int nilai = 0;
        if(rbPil1b.isChecked()) nilai++;
        if(rbPil2a.isChecked()) nilai++;
        if(rbPil3e.isChecked()) nilai++;
        if(rbPil4b.isChecked()) nilai++;
        if(rbPil5c.isChecked()) nilai++;
        if(rbPil6b.isChecked()) nilai++;
        if(rbPil7c.isChecked()) nilai++;

        if(nilai !=0) nilai = nilai +3;
        score = nilai * 10;

        return score;
    }

    private void kirimNilai(String nama, final int score)
    {
        String[] body = new String[2];
        body[0] = nama;
        body[1] = "" + score;

        new UpdateEvaluasi2(ActivityEvaluasi22.this, ActivityEvaluasi22.this, body, new InterfaceCheckIfValid() {
            @Override
            public void checkForRequest(Boolean condition) {
                if (condition)
                {
                    Toast.makeText(ActivityEvaluasi22.this, "Sukses menambahkan data.", Toast.LENGTH_SHORT).show();
                    CustomDialog dialog = new CustomDialog(ActivityEvaluasi22.this, "eva2");
                    dialog.dialogNilai(ActivityEvaluasi22.this, score);
                }
                else
                {
                    Toast.makeText(ActivityEvaluasi22.this,
                            "Gagal menambahkan data.\nSilahkan coba lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
