package com.example.ablmnewton.activities.evaluasi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;
import com.example.ablmnewton.interfaces.InterfaceCheckIfValid;
import com.example.ablmnewton.requests.CreateEvaluasi;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityEvaluasi21 extends AppCompatActivity {

    PDFView pdf1, pdf2, pdf3, pdf4, pdf5;
    RadioButton rbPil1a, rbPil1b, rbPil1c, rbPil1d, rbPil1e, rbPil2a, rbPil2b, rbPil2c, rbPil2d,
            rbPil2e, rbPil3a, rbPil3b, rbPil3c, rbPil3d, rbPil3e, rbPil4a, rbPil4b, rbPil4c,
            rbPil4d, rbPil4e, rbPil5a, rbPil5b, rbPil5c, rbPil5d, rbPil5e;
    Button btnSubmit;

    int score;
    private String nama, nis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_evaluasi_2_1);

        nama    = getIntent().getStringExtra("nama");
        nis     = getIntent().getStringExtra("nis");

        pdf1 = (PDFView) findViewById(R.id.pdf1);
        pdf2 = (PDFView) findViewById(R.id.pdf2);
        pdf3 = (PDFView) findViewById(R.id.pdf3);
        pdf4 = (PDFView) findViewById(R.id.pdf4);
        pdf5 = (PDFView) findViewById(R.id.pdf5);

        rbPil1a = findViewById(R.id.rb_pil_1_a);
        rbPil1b = findViewById(R.id.rb_pil_1_b);
        rbPil1c = findViewById(R.id.rb_pil_1_c);
        rbPil1d = findViewById(R.id.rb_pil_1_d);
        rbPil1e = findViewById(R.id.rb_pil_1_e);

        rbPil2a = findViewById(R.id.rb_pil_2_a);
        rbPil2b = findViewById(R.id.rb_pil_2_b);
        rbPil2c = findViewById(R.id.rb_pil_2_c);
        rbPil2d = findViewById(R.id.rb_pil_2_d);
        rbPil2e = findViewById(R.id.rb_pil_2_e);

        rbPil3a = findViewById(R.id.rb_pil_3_a);
        rbPil3b = findViewById(R.id.rb_pil_3_b);
        rbPil3c = findViewById(R.id.rb_pil_3_c);
        rbPil3d = findViewById(R.id.rb_pil_3_d);
        rbPil3e = findViewById(R.id.rb_pil_3_e);

        rbPil4a = findViewById(R.id.rb_pil_4_a);
        rbPil4b = findViewById(R.id.rb_pil_4_b);
        rbPil4c = findViewById(R.id.rb_pil_4_c);
        rbPil4d = findViewById(R.id.rb_pil_4_d);
        rbPil4e = findViewById(R.id.rb_pil_4_e);

        rbPil5a = findViewById(R.id.rb_pil_5_a);
        rbPil5b = findViewById(R.id.rb_pil_5_b);
        rbPil5c = findViewById(R.id.rb_pil_5_c);
        rbPil5d = findViewById(R.id.rb_pil_5_d);
        rbPil5e = findViewById(R.id.rb_pil_5_e);

        btnSubmit = (Button) findViewById(R.id.btn_submit);

        dataSoal();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSoal();

                score = nilai();

                kirimNilai(nama, nis, score);
            }
        });
    }

    private  void dataSoal(){
        pdf1.fromAsset("Latihan 1_1.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf2.fromAsset("Latihan 1_2.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf3.fromAsset("Latihan 1_3.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf4.fromAsset("Latihan 1_4.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        pdf5.fromAsset("Latihan 1_5.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
        dataPilihan();
    }

    private void dataPilihan(){
        rbPil1a.setText("A. (1) dan (2) saja");
        rbPil1b.setText("B. (1) dan (3) saja");
        rbPil1c.setText("C. (1) dan (4) saja");
        rbPil1d.setText("D. (1),(2) dan (3) saja");
        rbPil1e.setText("E. (1),(2),(3) dan (4)");

        rbPil2a.setText("A. dengan menarik taplak meja dengan cepat");
        rbPil2b.setText("B. dengan menarik taplak meja secara perlahan");
        rbPil2c.setText("C. dengan memukulkan koin lain dengan perlahan dan lemah yang mengarah ke koin paling bawah");
        rbPil2d.setText("D. dengan memukulkan koin lain dengan cepat dan keras mengarah ke koin paling bawah");
        rbPil2e.setText("E. dengan memukulkan koin lain dengan cepat dan keras ke arah tumpukan koin ");

        rbPil3a.setText("A. 15 N");
        rbPil3b.setText("B. 30 N");
        rbPil3c.setText("C. 45 N");
        rbPil3d.setText("D. 60 N");
        rbPil3e.setText("E. 90 N");

        rbPil4a.setText("A. 0");
        rbPil4b.setText("B. 10");
        rbPil4c.setText("C. 20");
        rbPil4d.setText("D. 30");
        rbPil4e.setText("E. 40");

        rbPil5a.setText("A. T1= 40√3 N & T2 = 40 N");
        rbPil5b.setText("B. T1= 40N & T2 = 40√3N");
        rbPil5c.setText("C. T1= 50N & T2 = 50 N");
        rbPil5d.setText("D. T1= 50√3 N & T2 = 50 N");
        rbPil5e.setText("E. T1= 50N & T2 = 50√3N");
    }

    private int nilai (){
        int nilai =0;
        if(rbPil1d.isChecked()) nilai++;
        if(rbPil2d.isChecked()) nilai++;
        if(rbPil3b.isChecked()) nilai++;
        if(rbPil4b.isChecked()) nilai++;
        if(rbPil5d.isChecked()) nilai++;

        score = nilai * 20;

        return score;
    }

    private void kirimNilai(String nama, String nis, final int score)
    {
        String[] body = new String[3];
        body[0] = nama;
        body[1] = nis;
        body[2] = "" + score;

        new CreateEvaluasi(ActivityEvaluasi21.this, ActivityEvaluasi21.this, body, new InterfaceCheckIfValid() {
            @Override
            public void checkForRequest(Boolean condition) {
                if (condition)
                {
                    Toast.makeText(ActivityEvaluasi21.this, "Sukses menambahkan data.", Toast.LENGTH_SHORT).show();
                    CustomDialog dialog = new CustomDialog(ActivityEvaluasi21.this, "eva1");
                    dialog.dialogNilai(ActivityEvaluasi21.this, score);
                }
                else
                {
                    Toast.makeText(ActivityEvaluasi21.this,
                            "Gagal menambahkan data.\nSilahkan coba lagi.", Toast.LENGTH_SHORT).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
