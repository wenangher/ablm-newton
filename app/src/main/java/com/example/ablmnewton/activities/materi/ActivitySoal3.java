package com.example.ablmnewton.activities.materi;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivitySoal3 extends AppCompatActivity {

    private TextView tvJudulSoal;
    private PDFView pdfSoal;
    private Button btnPenyelesaian;

    private int posisi;
    private String jenis;
    private String [] arrSoal, arrPenyelesaian;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_soal2);

        tvJudulSoal = (TextView) findViewById(R.id.tv_judulsoal);
        pdfSoal     = (PDFView) findViewById(R.id.pdfviewer);
        btnPenyelesaian = (Button) findViewById(R.id.btn_penyelesaian);

        posisi = getIntent().getIntExtra("posisi", 0);
        jenis = getIntent().getStringExtra("jenis");

        tvJudulSoal.setText("Contoh Soal " + (posisi+1));

        switch (jenis)
        {
            case "a":
                this.setTitle("Contoh Soal Hukum I Newton");
                arrSoal = getResources().getStringArray(R.array.contoh_soal1);
                arrPenyelesaian = getResources().getStringArray(R.array.penyelesaian_1);
                break;
            case "b":
                this.setTitle("Contoh Soal Hukum II Newton");
                arrSoal = getResources().getStringArray(R.array.contoh_soal2);
                arrPenyelesaian = getResources().getStringArray(R.array.penyelesaian_2);
                break;
            case "c":
                this.setTitle("Contoh Soal Hukum III Newton");
                arrSoal = getResources().getStringArray(R.array.contoh_soal3);
                arrPenyelesaian = getResources().getStringArray(R.array.penyelesaian_3);
                break;
        }

        displayFromAsset(pdfSoal, arrSoal[posisi]);

        btnPenyelesaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new CustomDialog(ActivitySoal3.this, arrPenyelesaian[posisi]).dialogPenyelesaian();
            }
        });
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }
}
