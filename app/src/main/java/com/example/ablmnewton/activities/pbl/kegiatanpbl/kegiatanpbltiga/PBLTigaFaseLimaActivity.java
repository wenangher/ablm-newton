package com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class PBLTigaFaseLimaActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    public static String USER_TYPE = "USER_TYPE";
    public static String EXTRA_DATA = "EXTRA_DATA";
    public static String USER = "USER";
    private EditText inputAnswer1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbltiga_fase_lima);

        String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        mDatabase = FirebaseDatabase.getInstance().getReference("KegiatanPBL");

        PDFView pdf = findViewById(R.id.pdf);
        PDFView pdf2 = findViewById(R.id.pdf2);
        inputAnswer1 = findViewById(R.id.answer1);
        Button submit = findViewById(R.id.btn_submit);

        pdf.fromAsset("PBL - 35.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf2.fromAsset("PBL - 35akhir.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        assert type != null;
        if (type.equals("GURU")) {
            DataHasil dataHasil = getIntent().getParcelableExtra(EXTRA_DATA);
            submit.setVisibility(View.INVISIBLE);
            inputAnswer1.setEnabled(false);
            inputAnswer1.setTextColor(getResources().getColor(R.color.black));
            assert dataHasil != null;
            inputAnswer1.setText(dataHasil.getAnwswer1());
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String answer = inputAnswer1.getText().toString();
                if (answer.equals(" ") || answer.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Anda belum menjawab", Toast.LENGTH_SHORT).show();
                } else {
                    sendAnswer(user, answer);
                }
            }
        });
    }

    private void sendAnswer(String users, final String answer) {
        Utils.showDialog(PBLTigaFaseLimaActivity.this);
        HashMap<String, String> maps = new HashMap<>();
        maps.put("user", users);
        maps.put("answer1", answer);
        maps.put("status", "0");
        maps.put("nilai", "");
        mDatabase.child("PBLTiga").child("FaseLima").push().setValue(maps).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getBaseContext(), "Berhasil mengirim data", Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }
}
