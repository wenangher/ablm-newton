package com.example.ablmnewton.activities.evaluasi;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityPenjelasanEvaluasi extends AppCompatActivity {

    PDFView pdfView;
    String evaluasi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.frag_pdf);

        pdfView = (PDFView) findViewById(R.id.pdfviewer);

        evaluasi = getIntent().getStringExtra("evaluasi");

        Log.d("eva", evaluasi);

        switch (evaluasi)
        {
            case "eva1":

                this.setTitle("Penjelasan Evaluasi 1");
                displayFromAsset(pdfView, "Penjelasan 1.pdf");

                break;
            case "eva2":

                this.setTitle("Penjelasan Evaluasi 2");
                displayFromAsset(pdfView, "Penjelasan 2.pdf");

                break;
            case "eva3":

                this.setTitle("Penjelasan Evaluasi 3");
                displayFromAsset(pdfView, "Penjelasan 3.pdf");

                break;
        }
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }
}
