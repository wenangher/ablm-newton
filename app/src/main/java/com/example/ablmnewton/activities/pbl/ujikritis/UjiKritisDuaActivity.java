package com.example.ablmnewton.activities.pbl.ujikritis;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.ablmnewton.BuildConfig;
import com.example.ablmnewton.R;
import com.example.ablmnewton.data.DataHasil;
import com.example.ablmnewton.data.Utils;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class UjiKritisDuaActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    public static String USER_TYPE = "USER_TYPE";
    public static String EXTRA_DATA = "EXTRA_DATA";
    public static String USER = "USER";
    private ImageView inputAnswer, inputAnswer3, inputAnswer4;
    private TextView inputAnswer2;
    private String mCurrentPhotoPath;
    private StorageReference storageRef;
    private Uri uri, uri3, uri4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uji_kritis_dua);

        String type = getIntent().getStringExtra(USER_TYPE);
        final String user = getIntent().getStringExtra(USER);

        mDatabase = FirebaseDatabase.getInstance().getReference("UjiKritis");
        storageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://ablm-newton.appspot.com");

        PDFView pdf = findViewById(R.id.pdf);
        PDFView pdf2 = findViewById(R.id.pdf2);
        PDFView pdf3 = findViewById(R.id.pdf3);
        PDFView pdf4 = findViewById(R.id.pdf4);
        inputAnswer = findViewById(R.id.answer1);
        inputAnswer2 = findViewById(R.id.answer2);
        inputAnswer3 = findViewById(R.id.answer3);
        inputAnswer4 = findViewById(R.id.answer4);
        Button submit = findViewById(R.id.btn_submit);


        pdf.fromAsset("Uji Kritis - 21.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf2.fromAsset("Uji Kritis - 22.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf3.fromAsset("Uji Kritis - 23.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        pdf4.fromAsset("Uji Kritis - 24.pdf")
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .enableSwipe(true)
                .load();

        Glide.with(UjiKritisDuaActivity.this)
                .load(R.drawable.upload_image)
                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(inputAnswer);

        Glide.with(UjiKritisDuaActivity.this)
                .load(R.drawable.upload_image)
                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(inputAnswer3);

        Glide.with(UjiKritisDuaActivity.this)
                .load(R.drawable.upload_image)
                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(inputAnswer4);

        assert type != null;
        if (type.equals("GURU")) {
            Utils.showDialog(this);
            final DataHasil dataHasil = getIntent().getParcelableExtra(EXTRA_DATA);
            submit.setVisibility(View.INVISIBLE);
            inputAnswer.setEnabled(false);
            inputAnswer2.setEnabled(false);
            inputAnswer3.setEnabled(false);
            inputAnswer4.setEnabled(false);
            inputAnswer2.setTextColor(getResources().getColor(R.color.black));
            assert dataHasil != null;
            inputAnswer2.setText(dataHasil.getAnwswer1());

            StorageReference pathReference1 = storageRef.child("UjiKritisDua").child(dataHasil.getId() + "1.png");
            pathReference1.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(UjiKritisDuaActivity.this)
                            .load(uri)
                            .error(R.drawable.ic_error)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(inputAnswer);

                    StorageReference pathReference2 = storageRef.child("UjiKritisDua").child(dataHasil.getId() + "2.png");
                    pathReference2.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(UjiKritisDuaActivity.this)
                                    .load(uri)
                                    .error(R.drawable.ic_error)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .into(inputAnswer3);

                            StorageReference pathReference3 = storageRef.child("UjiKritisDua").child(dataHasil.getId() + "3.png");
                            pathReference3.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Glide.with(UjiKritisDuaActivity.this)
                                            .load(uri)
                                            .error(R.drawable.ic_error)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .skipMemoryCache(true)
                                            .into(inputAnswer4);
                                    Utils.hideDialog();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Utils.hideDialog();
                                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Utils.hideDialog();
                            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Utils.hideDialog();
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

        inputAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    dispatchTakePictureIntent(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        inputAnswer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    dispatchTakePictureIntent(3);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        inputAnswer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    dispatchTakePictureIntent(4);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                String answer2 = inputAnswer2.getText().toString();
                if (uri == null || answer2.isEmpty() || uri3 == null || uri4 == null) {
                    Toast.makeText(getBaseContext(), "Anda belum menjawab", Toast.LENGTH_SHORT).show();
                } else {
                    sendAnswer(user, uri, answer2, uri3, uri4);
                }
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void sendAnswer(final String users, final Uri answer1, final String answer2, final Uri answer3, final Uri answer4) {
        Utils.showDialog(UjiKritisDuaActivity.this);
        final String key = mDatabase.child("UjiKritisDua").push().getKey();
        StorageReference path = storageRef.child("UjiKritisDua").child(key + "1.png");
        UploadTask uploadTask = path.putFile(answer1);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                StorageReference path = storageRef.child("UjiKritisDua").child(key + "2.png");
                UploadTask uploadTask = path.putFile(answer3);
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        StorageReference path = storageRef.child("UjiKritisDua").child(key + "3.png");
                        UploadTask uploadTask = path.putFile(answer4);
                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                HashMap<String, String> maps = new HashMap<>();
                                maps.put("user", users);
                                maps.put("answer1", answer2);
                                maps.put("nilai", "");
                                maps.put("status", "0");
                                assert key != null;
                                mDatabase.child("UjiKritisDua").child(key).setValue(maps).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(getBaseContext(), "Berhasil mengirim data", Toast.LENGTH_SHORT).show();
                                        Utils.hideDialog();
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                        Utils.hideDialog();
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                Utils.hideDialog();
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        Utils.hideDialog();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.hideDialog();
            }
        });
    }

    private void dispatchTakePictureIntent(int posisi) throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            File photoFile;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                return;
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, posisi);
            }
        }
    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri convert = Uri.parse(mCurrentPhotoPath);
                uri = Uri.fromFile(Utils.saveBitmapToFile(new File(Objects.requireNonNull(convert.getPath()))));
                displayImageOriginal(this, inputAnswer, uri);
            } else if (requestCode == 3) {
                Uri convert = Uri.parse(mCurrentPhotoPath);
                uri3 = Uri.fromFile(Utils.saveBitmapToFile(new File(Objects.requireNonNull(convert.getPath()))));
                displayImageOriginal(this, inputAnswer3, uri3);
            } else if (requestCode == 4) {
                Uri convert = Uri.parse(mCurrentPhotoPath);
                uri4 = Uri.fromFile(Utils.saveBitmapToFile(new File(Objects.requireNonNull(convert.getPath()))));
                displayImageOriginal(this, inputAnswer4, uri4);
            }
        }
    }


    public static void displayImageOriginal(Context ctx, ImageView img, Uri uri) {
        Glide.with(ctx)
                .load(uri)
                .error(R.drawable.ic_error)
                .centerInside()
                .transition(new DrawableTransitionOptions().crossFade(2000))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(img);
    }
}
