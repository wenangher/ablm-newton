package com.example.ablmnewton.activities.pbl.kegiatanpbl;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbldua.KegiatanPBLDuaActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpblsatu.KegiatanPBLSatuActivity;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.kegiatanpbltiga.KegiatanPBLTigaActivity;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class KegiatanPBLActivity extends AppCompatActivity {

    public static String USER_TYPE = "USER_TYPE";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_pbl);

        Button kegiatanPBLSatu = findViewById(R.id.btn_kegiatanpbl_1);
        Button kegiatanPBLDua = findViewById(R.id.btn_kegiatanpbl_2);
        Button kegiatanPBLTiga = findViewById(R.id.btn_kegiatanpbl_3);

        final String type = getIntent().getStringExtra(USER_TYPE);

        assert type != null;
        if(type.equals("GURU")){
            Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Kegiatan PBL");
        }

        kegiatanPBLSatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLSatuActivity.class);
                    intent.putExtra(KegiatanPBLSatuActivity.USER_TYPE,type);
                    startActivity(intent);
                }else{
                    createUser(1,type);
                }
            }
        });

        kegiatanPBLDua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLDuaActivity.class);
                    intent.putExtra(KegiatanPBLDuaActivity.USER_TYPE,type);
                    startActivity(intent);
                }else{
                    createUser(2,type);
                }
            }
        });

        kegiatanPBLTiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.equals("GURU")){
                    Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLTigaActivity.class);
                    intent.putExtra(KegiatanPBLTigaActivity.USER_TYPE,type);
                    startActivity(intent);
                }else{
                    createUser(3,type);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createUser(final int posisi, final String type){
        final Dialog dialog = new Dialog(KegiatanPBLActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_user_submit);
        dialog.setCancelable(true);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextInputEditText user = dialog.findViewById(R.id.user);
        Button send = dialog.findViewById(R.id.submit);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String users = Objects.requireNonNull(user.getText()).toString();
                if (users.equals(" ") || users.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Nama belum diisi", Toast.LENGTH_SHORT).show();
                } else {
                    if(posisi == 1){
                        Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLSatuActivity.class);
                        intent.putExtra(KegiatanPBLSatuActivity.USER_TYPE,type);
                        intent.putExtra(KegiatanPBLSatuActivity.USER,users);
                        startActivity(intent);
                        dialog.dismiss();
                    }else if(posisi == 2){
                        Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLDuaActivity.class);
                        intent.putExtra(KegiatanPBLDuaActivity.USER_TYPE,type);
                        intent.putExtra(KegiatanPBLDuaActivity.USER,users);
                        startActivity(intent);
                        dialog.dismiss();
                    }else if(posisi == 3){
                        Intent intent = new Intent(KegiatanPBLActivity.this,KegiatanPBLTigaActivity.class);
                        intent.putExtra(KegiatanPBLTigaActivity.USER_TYPE,type);
                        intent.putExtra(KegiatanPBLTigaActivity.USER,users);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
