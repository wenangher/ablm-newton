package com.example.ablmnewton.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.evaluasi.ActivityEvaluasi1;
import com.example.ablmnewton.activities.guru.ActivityGuru;

public class ActivityLogin extends AppCompatActivity {

    EditText edUsername, edPassword;
    Button btnLoginSiswa, btnLoginGuru;

    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_login);

        edUsername = (EditText) findViewById(R.id.ed_username);
        edPassword = (EditText) findViewById(R.id.ed_password);
        btnLoginGuru = (Button) findViewById(R.id.btn_login_guru);
        btnLoginSiswa = (Button) findViewById(R.id.btn_login_siswa);

        btnLoginSiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edUsername.getText().toString().equals("") || !edPassword.getText().toString().equals(""))
                {
                    intent = new Intent(ActivityLogin.this, ActivityEvaluasi1.class);
                    intent.putExtra("nama", edUsername.getText().toString());
                    intent.putExtra("nis", edPassword.getText().toString());
                    startActivity(intent);
                    finish();
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this, R.style.AlertDialogStyle);
                    builder.setMessage("Nama atau NIS tidak boleh kosong!");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        btnLoginGuru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edUsername.getText().toString().equals("") || !edPassword.getText().toString().equals(""))
                {
                    if (edUsername.getText().toString().equals("reni") && edPassword.getText().toString().equals("ablm2019"))
                    {
                        intent = new Intent(ActivityLogin.this, ActivityGuru.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this, R.style.AlertDialogStyle);
                        builder.setMessage("Username atau password salah!");
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this, R.style.AlertDialogStyle);
                    builder.setMessage("Username atau Password tidak boleh kosong!");
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }
}
