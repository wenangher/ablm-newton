package com.example.ablmnewton.activities.pbl;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.activities.materi.ActivityMateri1;
import com.example.ablmnewton.activities.pbl.kegiatanpbl.KegiatanPBLActivity;
import com.example.ablmnewton.activities.pbl.ujikritis.UjiKritisActivity;

public class ActivityKontenPBL extends AppCompatActivity {

    Button materi, realPBL, kegiatanPBL,ujiKritis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_kontenpbl);

        materi = (Button) findViewById(R.id.btn_materi);
        realPBL = (Button) findViewById(R.id.btn_realpbl);
        kegiatanPBL = (Button) findViewById(R.id.btn_kegiatanpbl);
        ujiKritis = findViewById(R.id.btn_ujikritis);

        realPBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActivityKontenPBL.this, ActivityRealPBL.class);
                startActivity(intent);
            }
        });

        materi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityKontenPBL.this, ActivityMateri1.class);
                startActivity(intent);
            }
        });

        kegiatanPBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityKontenPBL.this,KegiatanPBLActivity.class);
                intent.putExtra(KegiatanPBLActivity.USER_TYPE,"SISWA");
                startActivity(intent);

            }
        });

        ujiKritis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityKontenPBL.this,UjiKritisActivity.class);
                intent.putExtra(KegiatanPBLActivity.USER_TYPE,"SISWA");
                startActivity(intent);
            }
        });
    }
}
