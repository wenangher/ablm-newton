package com.example.ablmnewton.activities.pbl.ujikritis;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.ablmnewton.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class UjiKritisActivity extends AppCompatActivity {

    public static String USER_TYPE = "USER_TYPE";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uji_kritis);

        Button ujiKritisSatu = findViewById(R.id.btn_ujikritis_1);
        Button ujiKritisDua = findViewById(R.id.btn_ujikritis_2);
        Button ujiKritisTiga = findViewById(R.id.btn_ujikritis_3);

        final String type = getIntent().getStringExtra(USER_TYPE);

        assert type != null;
        if (type.equals("GURU")) {
            Objects.requireNonNull(getSupportActionBar()).setTitle("Evaluasi Uji Kritis");
        }

        ujiKritisSatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("GURU")) {
                    Intent intent = new Intent(UjiKritisActivity.this, EvaluasiUjiKritisActivity.class);
                    intent.putExtra(EvaluasiUjiKritisActivity.USER_TYPE, type);
                    intent.putExtra(EvaluasiUjiKritisActivity.KEGIATAN, 1);
                    startActivity(intent);
                } else {
                    createUser(1,type);
                }
            }
        });

        ujiKritisDua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("GURU")) {
                    Intent intent = new Intent(UjiKritisActivity.this, EvaluasiUjiKritisActivity.class);
                    intent.putExtra(EvaluasiUjiKritisActivity.USER_TYPE, type);
                    intent.putExtra(EvaluasiUjiKritisActivity.KEGIATAN, 2);
                    startActivity(intent);
                } else {
                    createUser(2,type);
                }
            }
        });

        ujiKritisTiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("GURU")) {
                    Intent intent = new Intent(UjiKritisActivity.this, EvaluasiUjiKritisActivity.class);
                    intent.putExtra(EvaluasiUjiKritisActivity.USER_TYPE, type);
                    intent.putExtra(EvaluasiUjiKritisActivity.KEGIATAN, 3);
                    startActivity(intent);
                } else {
                    createUser(3,type);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createUser(final int posisi, final String type){
        final Dialog dialog = new Dialog(UjiKritisActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_user_submit);
        dialog.setCancelable(true);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextInputEditText user = dialog.findViewById(R.id.user);
        Button send = dialog.findViewById(R.id.submit);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String users = Objects.requireNonNull(user.getText()).toString();
                if (users.equals(" ") || users.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Nama belum diisi", Toast.LENGTH_SHORT).show();
                } else {
                    if(posisi == 1){
                        Intent intent = new Intent(UjiKritisActivity.this, UjiKritisSatuActivity.class);
                        intent.putExtra(UjiKritisSatuActivity.USER_TYPE, type);
                        intent.putExtra(UjiKritisSatuActivity.USER, users);
                        startActivity(intent);
                        dialog.dismiss();
                    }else if(posisi == 2){
                        Intent intent = new Intent(UjiKritisActivity.this, UjiKritisDuaActivity.class);
                        intent.putExtra(UjiKritisDuaActivity.USER_TYPE, type);
                        intent.putExtra(UjiKritisDuaActivity.USER, users);
                        startActivity(intent);
                        dialog.dismiss();
                    }else if(posisi == 3){
                        Intent intent = new Intent(UjiKritisActivity.this, UjiKritisTigaActivity.class);
                        intent.putExtra(UjiKritisTigaActivity.USER_TYPE, type);
                        intent.putExtra(UjiKritisTigaActivity.USER, users);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
