package com.example.ablmnewton.activities.materi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ablmnewton.R;
import com.example.ablmnewton.dialogs.CustomDialog;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

public class ActivityMateri3 extends AppCompatActivity {

    private String[] arrFilePDF;
    private PDFView pdfView;
    private VideoView video;
    private Button btnVideo;
    private CustomDialog dialog;
    private String [] penjelasan;
    private int [] arrRawVideo;

    int posisi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_isimateri);

        pdfView = (PDFView) findViewById(R.id.pdf);
        btnVideo = (Button) findViewById(R.id.btn_video);

        this.setTitle(getIntent().getStringExtra("materi"));

        arrFilePDF = new String[] {"Materi Hukum I Newton.pdf", "Materi Hukum II Newton.pdf",
                                    "Materi Hukum III Newton.pdf"};

        penjelasan = new String[] {getResources().getString(R.string.penjelasan_video3),
                                   getResources().getString(R.string.penjelasan_video1)};

        arrRawVideo = new int[]{R.raw.menuangkan_saus, R.raw.mendorong_grobak, 0};

        posisi = getIntent().getIntExtra("posisi", 0);

        if (posisi == arrFilePDF.length)
        {
            Intent intent = new Intent(ActivityMateri3.this, ActivityMateri4.class);
            startActivity(intent);
            finish();
        }
        else
        {
            displayFromAsset(pdfView, arrFilePDF[posisi]);

            if (arrRawVideo[posisi] == 0)
            {
                btnVideo.setVisibility(View.GONE);
            }
            else
            {
                btnVideo.setVisibility(View.VISIBLE);
                btnVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog = new CustomDialog(ActivityMateri3.this, penjelasan[posisi]);
                        dialog.dialogVideo(arrRawVideo[posisi], ActivityMateri3.this);
                    }
                });
            }
        }
    }

    private void displayFromAsset(PDFView pdf, String assetFileName) {
        pdf.setMinZoom(1);
        pdf.fromAsset(assetFileName)
                .enableSwipe(true)
                .spacing(0)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }
}
