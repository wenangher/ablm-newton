package com.example.ablmnewton.requests;

import android.util.Log;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostRequests {

    String data;

    public String createEvaluasi (String url, String[] body)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("nama", body[0])
                .add("nis", body[1])
                .add("nilai", body[2])
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            data = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Data: ", data);
        return data;
    }

    public String updateEvaluasi2 (String url, String[] body)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("nama", body[0])
                .add("nilai2", body[1])
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            data = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Data: ", data);
        return data;
    }

    public String updateEvaluasi3 (String url, String[] body)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("nama", body[0])
                .add("nilai3", body[1])
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            data = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Data: ", data);
        return data;
    }

    public String getEvaAll (String url)
    {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            data = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Data: ", data);
        return data;
    }
}
