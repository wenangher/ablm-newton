package com.example.ablmnewton.requests;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.ablmnewton.R;
import com.example.ablmnewton.data.DataURI;
import com.example.ablmnewton.interfaces.InterfaceResultRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class GetEvaluasiAll extends AsyncTask<Void, Void, Boolean> {

    private String data;
    private String status;

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Boolean condition;

    private ProgressDialog progressDialog;

    JSONObject jsonData;

    private InterfaceResultRequest resultRequest;

    public GetEvaluasiAll(Activity activity, Context context,
                          InterfaceResultRequest resultRequest)
    {
        this.activity = activity;
        this.context = context;
        this.resultRequest = resultRequest;

        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("Loading data ..... Please wait.");
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        condition = false;

        PostRequests jsonConn = new PostRequests();

        String url = new DataURI().getURI_GET_EVA_ALL();
        data = jsonConn.getEvaAll(url);

        if (data != null) {

            try {
                jsonData = new JSONObject(data);

                if (jsonData.has("status"))
                {
                    status = jsonData.getString("status");

                    Log.d("data", "" + status);

                    condition = false;
                }
                else {
                    condition = true;
                }
            }
            catch (final JSONException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        else {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context,
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG).show();
                }
            });
        }

        return condition;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (aBoolean)
        {
            resultRequest.getData(jsonData);
        }
        else
        {
            AlertDialog dialog;
            AlertDialog.Builder dialogBuilder;

            dialogBuilder = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialogBuilder.setTitle("Error!");
            dialogBuilder.setMessage("JSON Parsing error.");
            dialog = dialogBuilder.create();
            dialog.show();
        }

    }
}
