package com.example.ablmnewton.requests;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.ablmnewton.data.DataURI;
import com.example.ablmnewton.interfaces.InterfaceCheckIfValid;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateEvaluasi extends AsyncTask<Void, Void, Boolean> {

    private String data;
    private String status;

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Boolean condition;

    private ProgressDialog progressDialog;

    JSONObject jsonData;

    private InterfaceCheckIfValid checkIfValid;

    String[] body;

    public CreateEvaluasi(Activity activity, Context context, String[] body,
                          InterfaceCheckIfValid checkIfValid)
    {
        this.activity = activity;
        this.context = context;
        this.body = body;
        this.checkIfValid = checkIfValid;

        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("Loading data ..... Please wait.");
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        condition = false;

        PostRequests jsonConn = new PostRequests();

        String url;

        url = new DataURI().getURI_CREATE_EVALUASI();
        data = jsonConn.createEvaluasi(url, body);

        if (data != null) {

            try {
                jsonData = new JSONObject(data);

                if (jsonData.has("status"))
                {
                    status = jsonData.getString("status");

                    switch (status)
                    {
                        case "sukses":
                            condition = true;
                            break;
                        case "gagal":
                            condition = false;
                            break;
                    }
                }
                else {
                    condition = false;
                }
            }
            catch (final JSONException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,
                                "Json parsing error: " + e.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        else {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context,
                            "Couldn't get json from server. Check LogCat for possible errors!",
                            Toast.LENGTH_LONG).show();
                }
            });
        }

        return condition;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        checkIfValid.checkForRequest(aBoolean);

    }
}
