package com.example.ablmnewton.interfaces;

import org.json.JSONObject;

public interface InterfaceResultRequest {

    void getData(JSONObject jsonData);
}