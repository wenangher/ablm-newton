package com.example.ablmnewton.data;

public class DataHasilEvaluasi {

    private String nama, nis;
    private int nilai1, nilai2, nilai3;

    public DataHasilEvaluasi(String nama, String nis,
                             int nilai1, int nilai2,
                             int nilai3)
    {
        this.nama = nama;
        this.nis = nis;
        this.nilai1 = nilai1;
        this.nilai2 = nilai2;
        this.nilai3 = nilai3;
    }

    public String getNama() {
        return nama;
    }

    public String getNis() {
        return nis;
    }

    public int getNilai1() {
        return nilai1;
    }

    public int getNilai2() {
        return nilai2;
    }

    public int getNilai3() {
        return nilai3;
    }
}
