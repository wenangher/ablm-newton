package com.example.ablmnewton.data;

import android.os.Parcel;
import android.os.Parcelable;

public class DataHasil implements Parcelable {
    private String id;
    private String nama;
    private String status;
    private String nilai;
    private String anwswer1;
    private String anwswer2;
    private String anwswer3;

    public DataHasil() {}

    protected DataHasil(Parcel in) {
        id = in.readString();
        nama = in.readString();
        status = in.readString();
        nilai = in.readString();
        anwswer1 = in.readString();
        anwswer2 = in.readString();
        anwswer3 = in.readString();
    }

    public static final Creator<DataHasil> CREATOR = new Creator<DataHasil>() {
        @Override
        public DataHasil createFromParcel(Parcel in) {
            return new DataHasil(in);
        }

        @Override
        public DataHasil[] newArray(int size) {
            return new DataHasil[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getAnwswer1() {
        return anwswer1;
    }

    public void setAnwswer1(String anwswer1) {
        this.anwswer1 = anwswer1;
    }

    public String getAnwswer2() {
        return anwswer2;
    }

    public void setAnwswer2(String anwswer2) {
        this.anwswer2 = anwswer2;
    }

    public String getAnwswer3() {
        return anwswer3;
    }

    public void setAnwswer3(String anwswer3) {
        this.anwswer3 = anwswer3;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(nama);
        parcel.writeString(status);
        parcel.writeString(nilai);
        parcel.writeString(anwswer1);
        parcel.writeString(anwswer2);
        parcel.writeString(anwswer3);
    }
}
