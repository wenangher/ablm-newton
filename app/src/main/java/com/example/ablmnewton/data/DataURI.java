package com.example.ablmnewton.data;

public class DataURI {

    private final String URI_BASE = "https://api.binakaryamultitrading.com/ablm/";
    //private final String URI_BASE = "http://192.168.139.95/api_inventory/";

    private final String URI_CREATE_EVALUASI = URI_BASE + "create_evaluasi.php";
    private final String URI_UPDATE_EVALUASI2 = URI_BASE + "update_evaluasi2.php";
    private final String URI_UPDATE_EVALUASI3 = URI_BASE + "update_evaluasi3.php";
    private final String URI_GET_EVA_ALL = URI_BASE + "get_evaluasi_all.php";

    public String getURI_CREATE_EVALUASI() {
        return URI_CREATE_EVALUASI;
    }

    public String getURI_UPDATE_EVALUASI2() {
        return URI_UPDATE_EVALUASI2;
    }

    public String getURI_UPDATE_EVALUASI3() {
        return URI_UPDATE_EVALUASI3;
    }

    public String getURI_GET_EVA_ALL() {
        return URI_GET_EVA_ALL;
    }
}
