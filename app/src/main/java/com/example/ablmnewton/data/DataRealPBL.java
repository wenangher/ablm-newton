package com.example.ablmnewton.data;

public class DataRealPBL {

    private String judul;
    private int gambarPBL;

    public DataRealPBL(String judul, int gambarPBL)
    {
        this.judul = judul;
        this.gambarPBL = gambarPBL;
    }

    public String getJudul() {
        return judul;
    }

    public int getGambarPBL() {
        return gambarPBL;
    }
}
